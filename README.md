

  ___________
  # ANGULAR-1.5-CLI
  ###### created by  Andrew Wormald

  ### Installation:


  npm install angular-1.5-cli -g

  You need to install this globally (aka using -g at the end) in order for this to work efficiently and enhance your experience.

  ___________

  ### Commands:

  #### Generate Project:

  gen new {{PROJECT NAME}}
  // Generates a new project using scss styling

  gen new {{PROJECT NAME}} --style:css

   // Generates a new project using css styling



  #### Generate Component:
  Make sure that you are at the base of the project directory

  gen {{COMPONENT NAME}}

  // Generates a new component using scss styling



  gen {{COMPONENT NAME}} --style:css

  // Generates a new component using css styling

  ___________
