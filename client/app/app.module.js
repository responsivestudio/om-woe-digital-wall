import 'bootstrap-css-only';
import 'normalize.css';
import angular from 'angular';
import appComponent from './app.component';
import ServicesModule from './services/services';
import ComponentsModule from './components/components';
import 'angular-sanitize';

angular.module('app', [
  ComponentsModule.name,
  ServicesModule.name,
  'ngSanitize'
])
  .component('app', appComponent);
        