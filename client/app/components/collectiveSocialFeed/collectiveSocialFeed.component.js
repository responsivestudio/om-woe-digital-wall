import template from './collectiveSocialFeed.component.html';
import controller from './collectiveSocialFeed.controller.js';
import './collectiveSocialFeed.component.scss';

let collectiveSocialFeedComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'collectiveSocialFeedController'
};
export default collectiveSocialFeedComponent;