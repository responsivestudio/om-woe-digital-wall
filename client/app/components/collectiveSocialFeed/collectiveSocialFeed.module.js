import angular from 'angular';
import collectiveSocialFeedComponent from './collectiveSocialFeed.component';
import 'angulargrid';

const collectiveSocialFeedModule = angular.module('collectiveSocialFeed', ['angularGrid'])
  .component('collectiveSocialFeed', collectiveSocialFeedComponent);
export default collectiveSocialFeedModule;