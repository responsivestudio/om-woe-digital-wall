class collectiveSocialFeedController {
  constructor($scope, $http, contentService, $q) {
    'ngInject';

    this.$scope = $scope;
    this.$http = $http;
    this.contentService = contentService;
    this.busyGettingData = false;
    this.lazyLoadPage = 1;
    this.lazyLoadAmount = 9;
    this.getData(this.lazyLoadPage, this.lazyLoadAmount);
    this.displayPopup = false;
    this.socialFeedData;
    this.selectedIndex = 0;
    this.currentURL = window.location.href;
    this.imageArray = ['0.jpg','1.jpg','2.jpg','3.jpg','4.jpg','5.jpg','6.jpg','7.jpg','8.jpg','9.jpg','10.jpg','11.jpg'];
  }

  getData(page, amount) {
    this.busyGettingData = true;
    this.contentService.getData(page, amount)
      .then((data) => {
        this.socialFeedData = data;
        this.busyGettingData = false;
      });
  };

  getData2(page, amount) {
    this.busyGettingData = true;
    this.contentService.getData(page, amount)
      .then((data) => {
        this.socialFeedData = this.socialFeedData.concat(data);
        this.busyGettingData = false;
      });
  };

  getTime(time) {
    var moment = require('moment');
    return moment(time, "YYYYMMDD").fromNow()
  }

  popupData(item) {
    this.selectedIndex = item;
  };

  togglePopup() {
    this.displayPopup = !this.displayPopup;
  };

  moveLeft() {
    if (this.selectedIndex === 0) {
      this.selectedIndex = (this.socialFeedData.length - 1)
    } else {
      this.selectedIndex = this.selectedIndex - 1;
    }
  };

  moveRight() {
    if (this.selectedIndex === (this.socialFeedData.length - 1)) {
      this.selectedIndex = 0;
    } else {
      this.selectedIndex = this.selectedIndex + 1;
    }
  };

  getNextBunchOfData() {
    this.lazyLoadAmount = 3;
    this.lazyLoad = (this.lazyLoad + 1);
    this.getData2(this.lazyLoadPage, this.lazyLoadAmount);
  }

  getRandomImage() {
    var i = Math.floor(Math.random() * this.imageArray.length);
    return this.imageArray[i];
  }

}

export default collectiveSocialFeedController;