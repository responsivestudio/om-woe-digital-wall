
    import angular from 'angular';
    import CollectiveSocialFeedModule from './collectiveSocialFeed/collectiveSocialFeed.module';

    const ComponentsModule = angular.module('app.components',[
     CollectiveSocialFeedModule.name 
    ]);

    export default ComponentsModule;

    