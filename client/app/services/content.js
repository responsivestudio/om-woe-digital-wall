class contentService {
  constructor($http, $q) {
    'ngInject';

    //INIT DEPENDENCIES
    this.$http = $http;
    this.$q = $q;

  }

  getData(page, amount) {
    const defer = this.$q.defer();

    this.$http.get(`https://www.juicer.io/api/feeds/getgreatadvice?per=${amount}&page=${page}`)
      .then((response) => {
        const data = response.data['posts']['items'];
        defer.resolve(data);
      })
      .catch((response) => {
        defer.reject(response.statusText);
      });
    return defer.promise;
  }
}

export default contentService;