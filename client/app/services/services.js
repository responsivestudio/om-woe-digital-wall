import angular from 'angular';
import contentService from './content';

const ServicesModule = angular.module('app.services', [])
  .service('contentService', contentService);

export default ServicesModule;

